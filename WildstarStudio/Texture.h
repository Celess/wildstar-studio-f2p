#pragma once

#include "Archive.h"

SHARED_FWD(Texture);


class Texture
{
	struct TexHeader			// header from the archive for the file
	{
		uint32 ident;			//const int headerId = 0x00474658;				// X, F, G, \0 (GFX)
		uint32 type;
		uint32 width;
		uint32 height;
		uint32 unk1;
		uint32 unk2;
		uint32 mips;
		uint32 format;
	};

	GLuint mTexture;
	FileEntryPtr mFile;
	TexHeader mHeader;
	std::shared_ptr<BinStreamSafe> mStream;


	bool EnsureHeader(BinStreamSafe& strm, TexHeader& header);

	bool loadMemory(std::vector<uint32>& outData);
	void loadDXT1(std::vector<uint32>& outData);
	void loadDXT3(std::vector<uint32>& outData);
	void loadDXT5(std::vector<uint32>& outData);

	void dxt1GetBlock(uint32* colorPtr);
	void dxt3GetBlock(uint32* colorPtr);
	void dxt5GetBlock(uint32* colorPtr);

	Texture();

public:
	Texture(FileEntryPtr file);

	static TexturePtr fromMemory(uint32 w, uint32 h, const std::vector<uint32>& colors);

	void exportAsPng();

	//void saveToFile(const std::wstring& file);
	void setRepeat();
	void setAndGenMipmap();

	bool loadTexture();
	GLuint getId() const { return mTexture; }
	FileEntryPtr getFile() const { return mFile; }

	const TexHeader& getHeader() const { return mHeader; }

	std::shared_ptr<Gdiplus::Bitmap> getBitmap();
};

SHARED_TYPE(Texture);