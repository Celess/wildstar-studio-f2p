#include "stdafx.h"
#include "FunctionInstance.h"

FunctionInstance::FunctionInstance() {
	v8::Isolate* isolate = v8::Isolate::GetCurrent();
	mFunction = v8::FunctionTemplate::New(isolate, &FunctionInstance::internalFunctionCallback, v8::External::New(isolate, this));
}

void FunctionInstance::internalFunctionCallback(const v8::FunctionCallbackInfo<v8::Value>& info) {
	v8::Isolate* isolate = v8::Isolate::GetCurrent();

	if (info.Data()->IsExternal() == false) {
		isolate->ThrowException(v8::String::NewFromUtf8(isolate, "internal error: native function call does not provide an external value."));
		return;
	}

	auto ext = info.Data().As<v8::External>();
	if (ext->Value() == nullptr) {
		isolate->ThrowException(v8::String::NewFromUtf8(isolate, "internal error: native function call does not provide an external value."));
		return;
	}

	FunctionInstance* funcInstance = (FunctionInstance*) ext->Value();
	funcInstance->invokeFunction(info);
}

void FunctionInstance::invokeFunction(const v8::FunctionCallbackInfo<v8::Value>& info) {
	for (auto& cst : mConstraints) {
		if (cst(info) == false) {
			return;
		}
	}

	std::shared_ptr<GenericFunctionOverload> bestMatch;
	uint32 bestScore = 0;

	for (auto& overload : mOverloads) {
		uint32 score = overload->matchScore(info);
		if (score == 0) {
			continue;
		}

		if (bestMatch == nullptr || score > bestScore) {
			bestScore = score;
			bestMatch = overload;
		}
	}

	if (bestMatch == nullptr) {
		v8::Isolate* isolate = v8::Isolate::GetCurrent();
		isolate->ThrowException(v8::Exception::TypeError(v8::String::NewFromUtf8(isolate, "Could not match function overload for function.")));
		return;
	}

	bestMatch->execute(info);
}