#include "stdafx.h"
#include "Function.h"

Function::Function(const std::wstring& name) {
	mName = name;

	mInstance = new FunctionInstance();
}

Function::~Function() {

}

Function::Function(const Function& ) {

}

void Function::onRegister(v8::Handle<v8::ObjectTemplate> templ) {
	v8::Isolate* isolate = v8::Isolate::GetCurrent();

	v8::Local<v8::String> name2;
	v8::String::NewFromTwoByte(isolate, (const uint16_t*)mName.c_str(), v8::NewStringType::kNormal, mName.length()).ToLocal(&name2);

	templ->Set(name2, mInstance->getTemplate());
	//templ->Set(v8::String::New((const uint16_t*)mName.c_str(), mName.length()), mInstance->getTemplate());
}