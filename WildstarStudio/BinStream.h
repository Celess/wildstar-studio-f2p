#pragma once

class BinStream
{
	const std::vector<uint8>& mData;
	uint32 mCurPos = 0;

public:
	BinStream(const std::vector<uint8>& data) : mData(data) {

	}

	void seek(uint64 pos) {
		mCurPos = (uint32)pos;
	}

	void seekMod(int32 mod) {
		mCurPos += mod;
	}

	uint32 tell() const { return mCurPos; }
	uint64 size() const { return mData.size(); }

	template<typename T> T read() {
		T ret;
		memcpy(&ret, &mData[mCurPos], sizeof(T));
		mCurPos += sizeof(T);
		return ret;
	}

	void read(void* buffer, uint64 numBytes) {
		memcpy(buffer, &mData[mCurPos], (uint32)numBytes);
		mCurPos += (uint32)numBytes;
	}

	const void* getPointer(uint64 offset) const {
		return &mData[(size_t) offset];
	}
};

class BinStreamSafe
{
	const std::vector<uint8>& mData;
	int64 mCurPos = 0;

public:
	BinStreamSafe(const std::vector<uint8>& data) : mData(data) {

	}

	bool seek(int64 pos) {
		if (pos < 0)
			return false;
		mCurPos = pos;
		return pos < size();
	}

	void seekMod(int64 mod) {
		mCurPos += mod;
	}

	int64 tell() const { return mCurPos; }
	int64 size() const { return mData.size(); }

	template<typename T> bool read(T& ret) {
		return read(&ret, sizeof(T)) == sizeof(T);
	}

	size_t read(void* buffer, size_t numBytes) {
		auto remainder = size() - mCurPos;
		if (mCurPos < 0) return 0;
		if (remainder < numBytes) numBytes = (size_t)remainder;

		memcpy(buffer, &mData[(size_t)mCurPos], numBytes);
		mCurPos += numBytes;
		return numBytes;
	}
};