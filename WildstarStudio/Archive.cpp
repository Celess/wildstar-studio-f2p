#include "stdafx.h"
#include "Archive.h"
#include "IOManager.h"

//namespace std
//{
//	namespace tr2
//	{
//		namespace sys
//		{
//			//struct wpath_traits;
//			typedef basic_path<std::wstring, std::wpath_traits>  wpath;
//		}
//	}
//}
//struct std::tr2::sys::wpath_traits;
//typedef std::tr2::sys::basic_path<std::wstring, wpath_traits>  wpath;

Archive::Archive(const std::wstring& file) {
	mIndexFile.open(file, std::ios::binary);
	if (mIndexFile.is_open() == false) {
		throw std::invalid_argument("Unable to open the file");
	}

	std::tr2::sys::path p(file);
	//std::tr2::sys::wpath p(file);
	std::wstringstream strm;
	strm << p.replace_extension(L"").generic_wstring() << L".archive";
	//strm << p.replace_extension(L"").string() << L".archive";

	mPackFile.open(strm.str(), std::ios::binary);
	if (mPackFile.is_open() == false) {
		throw std::invalid_argument("Unable to open archive file");
	}
}

void Archive::loadIndexInfo() {
	idxSeek(0);
	uint32 sig = idxRead<uint32>();
	if (sig != FileMagic) {
		throw std::runtime_error("Invalid file format");
	}

	uint32 version = idxRead<uint32>();
	if (version != 1) {
		throw std::runtime_error("Invalid file format");
	}

	uint8 sizeData[548];
	mIndexFile.read((char*) sizeData, 548);

	mDirectoryCount = *(uint32*) (sizeData + 536);
	mDirectoryTableStart = *(uint64*) (sizeData + 528);

	mDirectoryHeaders.resize(mDirectoryCount);
	idxSeek(mDirectoryTableStart);
	mIndexFile.read((char*) mDirectoryHeaders.data(), mDirectoryHeaders.size() * sizeof(PackDirectoryHeader));

	loadIndexTree();
}

void Archive::loadArchiveInfo() {
	pkSeek(0);

	uint32 sig = pkRead<uint32>();
	if (sig != FileMagicArchive) {
		throw std::runtime_error("Invalid file format");
	}

	uint32 version = pkRead<uint32>();
	if (version != 1) {
		throw std::runtime_error("Invalid file format");
	}

	uint8 sizeData[548];
	mPackFile.read((char*) sizeData, 548);

	mPkDirCount = *(uint32*) (sizeData + 536);
	mPkDirStart = *(uint64*) (sizeData + 528);

	mPkDirectoryHeaders.resize(mPkDirCount);
	pkSeek(mPkDirStart);
	pkRead(mPkDirectoryHeaders.data(), mPkDirCount * sizeof(PackDirectoryHeader));

	bool hasAarc = false;
	uint32 aarcBlock = 0;
	uint32 aarcEntries = 0;

	for (auto header : mPkDirectoryHeaders) {
		pkSeek(header.directoryOffset);
		if (header.blockSize < 16)
			continue;

		uint32 signature = pkRead<uint32>();
		uint32 version = pkRead<uint32>();
		uint32 craaEntryCount = pkRead<uint32>();
		uint32 craaTableBlock = pkRead<uint32>();

		if (signature == AarcMagic) {
			hasAarc = true;
			aarcBlock = craaTableBlock;
			aarcEntries = craaEntryCount;
			break;
		}
	}

	if (!hasAarc) {
		throw std::exception("Missing CRAA table in file");
	}

	pkSeek(mPkDirectoryHeaders[aarcBlock].directoryOffset);
	mAarcTable.resize(aarcEntries);
	pkRead(mAarcTable.data(), mAarcTable.size() * sizeof(AARCEntry));
}

void Archive::loadIndexTree() {
	bool aidxFound = false;

	for (auto& entry : mDirectoryHeaders) {
		if (entry.blockSize < sizeof(AIDX)) {
			continue;
		}

		idxSeek(entry.directoryOffset);
		AIDX idx = idxRead<AIDX>();
		if (idx.magic == IndexMagic) {
			aidxFound = true;
			mIndexHeader = idx;
			break;
		}
	}

	if (aidxFound == false) {
		throw std::runtime_error("Invalid file format, missing AIDX block");
	}

	mFileRoot = std::make_shared<DirectoryEntry>(std::shared_ptr<IFileSystemEntry>(), L"", mIndexHeader.rootBlock, shared_from_this());
	mFileCount = mFileRoot->countChildren();
	mFileCount += 1;
}

void Archive::asyncLoad() {
	mFileRoot->parseChildren();
}

IFileSystemEntryPtr Archive::getByPath(const std::wstring& path) const {
	return mFileRoot->getFile(path);
}

DirectoryEntry::DirectoryEntry(IFileSystemEntryPtr parent, const std::wstring& name, uint32 nextBlock, ArchivePtr archive) {
	mArchive = archive;
	mEntryName = name;
	mNextBlock = nextBlock;
	mParent = parent;
}

uint32 DirectoryEntry::countChildren() {
	auto& blockTable = mArchive->getBlockTable();

	auto nextBlock = blockTable[mNextBlock];

	mArchive->idxSeek(nextBlock.directoryOffset);

	uint32 numDirs = mArchive->idxRead<uint32>();
	uint32 numFiles = mArchive->idxRead<uint32>();

	std::list<DirectoryEntryPtr> dirEnts;

	for (uint32 i = 0; i < numDirs; ++i) {
		uint32 nameOffset = mArchive->idxRead<uint32>();
		uint32 nextBlock = mArchive->idxRead<uint32>();

		DirectoryEntryPtr dirEnt = std::make_shared<DirectoryEntry>(shared_from_this(), L"", nextBlock, mArchive);
		dirEnts.push_back(dirEnt);
	}

	for (auto& dirEnt : dirEnts) {
		numFiles += dirEnt->countChildren();
	}

	return numFiles + numDirs;
}

void DirectoryEntry::getFiles(std::vector<IFileSystemEntryPtr>& files) {
	for (auto entry : mChildren) {
		if (entry->isDirectory() == false) {
			files.push_back(entry);
		} else {
			std::dynamic_pointer_cast<DirectoryEntry>(entry)->getFiles(files);
		}
	}
}

void DirectoryEntry::getEntries(std::list<IFileSystemEntryPtr>& entries, bool recursive) {
	for (auto entry : mChildren) {
		entries.push_back(entry);
		if (recursive && entry->isDirectory() == true) {
			std::dynamic_pointer_cast<DirectoryEntry>(entry)->getEntries(entries, recursive);
		}
	}
}

IFileSystemEntryPtr DirectoryEntry::getFile(std::wstring path) {
	std::wstring::size_type t = path.find(L'\\');
	if (t == std::wstring::npos) {
		for (auto child : mChildren) {
			if (child->getEntryName() == path) {
				return child;
			}
		}

		return nullptr;
	}

	std::wstring dir = path.substr(0, t);
	std::wstring remain = path.substr(t + 1);
	for (auto child : mChildren) {
		if (child->getEntryName() == dir && child->isDirectory())
			return std::dynamic_pointer_cast<DirectoryEntry>(child)->getFile(remain);
	}

	return nullptr;
}

void DirectoryEntry::dumpFiles(std::wstring basePath, std::wostream& strm) {
	std::wstringstream newPath;
	newPath << basePath << mEntryName;
	if (mParent != nullptr) {
		newPath << L"\\";
	}

	for (auto child : mChildren) {
		child->dumpFiles(newPath.str(), strm);
	}
}

void DirectoryEntry::parseChildren() {
	auto& blockTable = mArchive->getBlockTable();

	auto nextBlock = blockTable[mNextBlock];

	mArchive->idxSeek(nextBlock.directoryOffset);
	uint32 numDirectories = mArchive->idxRead<uint32>();
	uint32 numFiles = mArchive->idxRead<uint32>();
	uint64 curPos = mArchive->idxTell();

	int64 dataSize = numDirectories * 8 + numFiles * 56;
	mArchive->idxSeekMod(dataSize);
	uint64 stringSize = nextBlock.blockSize - 8 - dataSize;

	std::vector<char> nameData((uint32)stringSize);
	const auto data = nameData.data();
	const auto size = nameData.size();

	mArchive->idxRead(data, size);

	mArchive->idxSeek(curPos);
	std::list<DirectoryEntryPtr> dirEntries;

	std::wstring wname;

	for (uint32 i = 0; i < numDirectories; ++i) {
		uint32 nameOffset = mArchive->idxRead<uint32>();
		uint32 nextBlock = mArchive->idxRead<uint32>();

		WSS_ToUnicode(wname, data + nameOffset);

		DirectoryEntryPtr dirEnt = std::make_shared<DirectoryEntry>(shared_from_this(), wname, nextBlock, mArchive);
		dirEntries.push_back(dirEnt);
		mChildren.push_back(dirEnt);
	}

	std::vector<uint8> junk(52);
	const auto junkData = junk.data();
	const auto junkSize = junk.size();

	for (uint32 i = 0; i < numFiles; ++i) {
		uint32 nameOffset = mArchive->idxRead<uint32>();
		mArchive->idxRead(junkData, junkSize);

		WSS_ToUnicode(wname, data + nameOffset);

		FileEntryPtr fe = std::make_shared<FileEntry>(shared_from_this(), wname, junk);
		mChildren.push_back(fe);
		sIOMgr->asyncFileLoaded();
	}

	for (auto dirEnt : dirEntries) {
		dirEnt->parseChildren();
	}

	sIOMgr->asyncFileLoaded();
}

void Archive::getFileData(FileEntryPtr file, std::vector<uint8>& outdata) {		// need to keep the stack a bit more unwound

	const auto hash = file->getHash();
	const auto flags = file->getFlags();

	// find entry

	int count = (int)mAarcTable.size(), i = 0;
	auto pentry = count ? &(mAarcTable[0]) : NULL;

	for (; i < count; i++,pentry++) {
		if (memcmp(pentry->shaHash, hash, 20) == 0) break;
	}
	if (i >= count) return;

	const AARCEntry& entry = *pentry;

	// get data

	auto block = mPkDirectoryHeaders[entry.blockIndex];
	pkSeek(block.directoryOffset);

	const auto blockSize = (size_t)block.blockSize;
	const auto uncompSize = (size_t)file->getSizeUncompressed();

	if (flags == 1) {
		outdata.resize(blockSize);
		pkRead(outdata.data(), blockSize);
		//outdata.assign(compressed.begin(), compressed.end());
	
	} else if (flags == 3) {
		std::vector<uint8> compressed(blockSize);
		pkRead(compressed.data(), blockSize);

		outdata.resize(uncompSize);

		z_stream strm = { 0 };
		strm.zalloc = Z_NULL;
		strm.zfree = Z_NULL;
		strm.opaque = Z_NULL;
		strm.avail_in = blockSize;
		strm.next_in = compressed.data();
		strm.avail_out = (uint32)outdata.size();
		strm.next_out = outdata.data();

		inflateInit(&strm);

		auto ret = inflate(&strm, Z_NO_FLUSH);
		if (ret == Z_STREAM_ERROR) {
			throw std::exception();
		}

		uint64 size = outdata.size() - strm.avail_out;
		outdata.resize((uint32)size);
		inflateEnd(&strm);
	}

	else if (flags == 5) {
		std::vector<uint8> compressed(blockSize);
		pkRead(compressed.data(), blockSize);

		outdata.resize(uncompSize);

		unsigned srcLen = blockSize - LZMA_PROPS_SIZE;
		unsigned dstLen = outdata.size();

		SRes res = LzmaUncompress(
			&outdata[0], &dstLen,
			&compressed[LZMA_PROPS_SIZE], &srcLen,
			&compressed[0], LZMA_PROPS_SIZE);
		
		if (res != SZ_OK) {
			int i = 0;
		}
		//assert(res == SZ_OK);
		
		outdata.resize(dstLen); // If uncompressed data can be smaller
	}

	else {
		outdata.resize(blockSize);
		pkRead(outdata.data(), blockSize);
		//outdata.assign(compressed.begin(), compressed.end());
	}
}

