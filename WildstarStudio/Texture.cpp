#include "stdafx.h"
#include "Texture.h"
#include "IOManager.h"
#include "UIManager.h"


Texture::Texture() {
	glGenTextures(1, &mTexture);
	glBindTexture(GL_TEXTURE_2D, mTexture);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glBindTexture(GL_TEXTURE_2D, 0);
}

Texture::Texture(FileEntryPtr file) {
	mFile = file;
	mTexture = 0;
}

void Texture::setRepeat() {
	glBindTexture(GL_TEXTURE_2D, mTexture);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glBindTexture(GL_TEXTURE_2D, 0);
}

void Texture::setAndGenMipmap() {
	glBindTexture(GL_TEXTURE_2D, mTexture);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glBindTexture(GL_TEXTURE_2D, 0);
}

TexturePtr Texture::fromMemory(uint32 w, uint32 h, const std::vector<uint32>& colors) {
	TexturePtr tex = std::shared_ptr<Texture>(new Texture());
	glBindTexture(GL_TEXTURE_2D, tex->getId());
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, w, h, 0, GL_RGBA, GL_UNSIGNED_BYTE, colors.data());
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glBindTexture(GL_TEXTURE_2D, 0);

	return tex;
}

bool Texture::EnsureHeader(BinStreamSafe& strm, TexHeader& header) {
	uint32 headerId = 0x00474658;					// \0,X,F,G - GFX
	uint32 headerSize = sizeof(TexHeader);

	if (!strm.read(header) ||						// read same size as header
		header.ident != headerId)					// correct signature
		return false;

	auto enc = header.format;
	if (//header.type != 0 ||						// only type 0 is known
		(enc != 0 && enc != 1 &&					// only support raw uncompressed or
			enc != 13 && enc != 14 && enc != 15))	// support dxt1, dxt3, dxt5
		return false;

	return true;
}

bool Texture::loadTexture() {
	// load 
	std::vector<uint8> fileData;
	sIOMgr->getArchive()->getFileData(mFile, fileData);
	BinStreamSafe strm(fileData);

	// validate header
	if (!EnsureHeader(strm, mHeader)) {
		return false;
	}

	// process header
	GLenum format;
	bool bIsCompressed = false;
	switch (mHeader.format) {
	case 0: format = GL_BGRA; break;
	case 1: format = GL_RGBA; break;
	case 13: format = GL_COMPRESSED_RGB_S3TC_DXT1_EXT; bIsCompressed = true; break;
	case 14: format = GL_COMPRESSED_RGBA_S3TC_DXT3_EXT; bIsCompressed = true; break;
	case 15: format = GL_COMPRESSED_RGBA_S3TC_DXT5_EXT; bIsCompressed = true; break;
	default: return false;
	}

	auto width = mHeader.width;
	auto height = mHeader.height;
	auto mips = mHeader.mips;

	auto factor = (!bIsCompressed) ? 4 : (format == GL_COMPRESSED_RGB_S3TC_DXT1_EXT) ? 0.5f : 1;
	auto length = (uint32)(width * height * factor);

	// validate payload
	auto start = (int)strm.tell();
	auto offset = (int)strm.size() - (int)length;			// first offset, largest map from end
	if (offset < start || !strm.seek(offset)) {				// validate stream for first mip
		return false;
	}

	// init texture
	glGenTextures(1, &mTexture);
	glBindTexture(GL_TEXTURE_2D, mTexture);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);

	// load mips
	std::vector<uint8> buffer(length);
	auto data = buffer.data();

	for (auto i = 0u; i < mips; i++) {
		strm.read(data, length);

		if (!bIsCompressed) {
			glTexImage2D(GL_TEXTURE_2D, i, GL_RGBA, width, height, 0, format, GL_UNSIGNED_BYTE, data);
		} else {
			glCompressedTexImage2D(GL_TEXTURE_2D, i, format, width, height, 0, length, data);
		}

		// validate next mip 
		width >>= 1;
		height >>= 1;
		if (width < 1) width = 1;
		if (height < 1) height = 1;
		length = (uint32)(width * height * factor);
		offset -= length;
		if (length < 1 || offset < start || !strm.seek(offset)) break;
	}

	glBindTexture(GL_TEXTURE_2D, 0);
	return true;
}

bool Texture::loadMemory(std::vector<uint32>& outData) {

	//if (mFile->getFlags() == 5)
	//	return false;

	// load 
	std::vector<uint8> fileData;
	sIOMgr->getArchive()->getFileData(mFile, fileData);
	mStream = std::make_shared<BinStreamSafe>(fileData);
	auto& strm = *mStream;

	// validate header
	if (!EnsureHeader(strm, mHeader)) {
		return false;
	}

	// process header
	GLenum format;
	bool bIsCompressed = false;
	switch (mHeader.format) {
	case 0: format = GL_BGRA; break;
	case 1: format = GL_RGBA; break;
	case 13: format = GL_COMPRESSED_RGB_S3TC_DXT1_EXT; bIsCompressed = true; break;
	case 14: format = GL_COMPRESSED_RGBA_S3TC_DXT3_EXT; bIsCompressed = true; break;
	case 15: format = GL_COMPRESSED_RGBA_S3TC_DXT5_EXT; bIsCompressed = true; break;
	default: return false;
	}

	auto width = mHeader.width;
	auto height = mHeader.height;
	auto mips = mHeader.mips;

	auto factor = (!bIsCompressed) ? 4 : (format == GL_COMPRESSED_RGB_S3TC_DXT1_EXT) ? 0.5f : 1;
	auto length = (uint32)(width * height * factor);

	// validate payload
	auto start = (int)strm.tell();
	auto offset = (int)strm.size() - (int)length;			// first offset, largest map from end
	if (offset < start || !strm.seek(offset)) {								// validate stream for first mip
		return false;
	}

	auto size = width * height;
	outData.resize(size);
	auto data = outData.data();

	switch (format) {
	case GL_COMPRESSED_RGB_S3TC_DXT1_EXT:
		loadDXT1(outData);
		break;

	case GL_COMPRESSED_RGBA_S3TC_DXT3_EXT:
		loadDXT3(outData);
		break;

	case GL_COMPRESSED_RGBA_S3TC_DXT5_EXT:
		loadDXT5(outData);
		break;

	case GL_RGB:
	case GL_RGBA:
	case GL_R:
	case GL_BGRA:
		strm.read(data, size);
		break;

	default:
		return false;
	}

	return true;
}


static void RGB565ToRGB8Array(uint16 input, uint8* output)
{
	uint32 r = (uint32)(input & 0x1F);
	uint32 g = (uint32)((input >> 5) & 0x3F);
	uint32 b = (uint32)((input >> 11) & 0x1F);

	r = (r << 3) | (r >> 2);
	g = (g << 2) | (g >> 4);
	b = (b << 3) | (b >> 2);

	output[0] = r;
	output[1] = g;
	output[2] = b;
}

int GetEncoderClsid(const WCHAR* format, CLSID* pClsid)
{
	using namespace Gdiplus;

	UINT  num = 0;
	UINT  size = 0;

	ImageCodecInfo* pImageCodecInfo = NULL;

	GetImageEncodersSize(&num, &size);
	if (size == 0)
		return -1;

	pImageCodecInfo = (ImageCodecInfo*)(malloc(size));
	if (pImageCodecInfo == NULL)
		return -1;

	GetImageEncoders(num, size, pImageCodecInfo);

	for (UINT j = 0; j < num; ++j)
	{
		if (wcscmp(pImageCodecInfo[j].MimeType, format) == 0)
		{
			*pClsid = pImageCodecInfo[j].Clsid;
			free(pImageCodecInfo);
			return j;
		}
	}

	free(pImageCodecInfo);
	return -1;
}

void Texture::exportAsPng() {
	std::vector<uint8> imageData(mHeader.width * mHeader.height * 4);

	glBindTexture(GL_TEXTURE_2D, mTexture);
	glGetTexImage(GL_TEXTURE_2D, 0, GL_BGRA, GL_UNSIGNED_BYTE, imageData.data());
	glBindTexture(GL_TEXTURE_2D, 0);

	std::async(std::launch::async, [this, imageData]() {
		std::wstringstream strm;
		strm << sIOMgr->getExtractionPath() << mFile->getFullPath() << L".bmp";

		std::wstring dir = std::tr2::sys::path(strm.str()).parent_path();
		//std::wstring dir = std::tr2::sys::wpath(strm.str()).branch_path();

		SHCreateDirectoryEx(nullptr, dir.c_str(), nullptr);

		Gdiplus::Bitmap bmp(mHeader.width, mHeader.height);
		Gdiplus::BitmapData bmpData;
		bmp.LockBits(&Gdiplus::Rect(0, 0, mHeader.width, mHeader.height), 0, PixelFormat32bppARGB, &bmpData);
		memcpy(bmpData.Scan0, imageData.data(), imageData.size());
		bmp.UnlockBits(&bmpData);

		CLSID idPng;
		GetEncoderClsid(L"image/bmp", &idPng);
		bmp.Save(strm.str().c_str(), &idPng);

		sUIMgr->asyncExtractComplete();
	});
}

std::shared_ptr<Gdiplus::Bitmap> Texture::getBitmap() {
	std::vector<uint32> clrData;

	if (!loadMemory(clrData))
		return NULL;

	auto bmp = std::make_shared<Gdiplus::Bitmap>(mHeader.width, mHeader.height, PixelFormat32bppARGB);
	Gdiplus::BitmapData data;
	bmp->LockBits(&Gdiplus::Rect(0, 0, mHeader.width, mHeader.height), 0, PixelFormat32bppARGB, &data);
	memcpy(data.Scan0, clrData.data(), clrData.size() * 4);
	bmp->UnlockBits(&data);

	return bmp;
}

void Texture::loadDXT1(std::vector<uint32>& outData) {
	int numBlocksFull = (mHeader.width * mHeader.height) / 16;
	bool partialBlock = ((mHeader.width * mHeader.height) % 16) != 0;

	std::vector<uint32> blockData((numBlocksFull + (partialBlock ? 1 : 0)) * 16);

	for (int32 i = 0; i < numBlocksFull; ++i) {
		dxt1GetBlock( blockData.data() + i * 16);
	}

	if (partialBlock) {
		dxt1GetBlock(blockData.data() + numBlocksFull * 16);
	}

	for (uint32 y = 0; y < mHeader.height; ++y) {
		for (uint32 x = 0; x < mHeader.width; ++x) {
			int bx = x / 4;
			int by = y / 4;

			int ibx = x % 4;
			int iby = y % 4;

			int blockIndex = by * (mHeader.width / 4) + bx;
			int innerIndex = iby * 4 + ibx;
			outData[y * mHeader.width + x] = blockData[blockIndex * 16 + innerIndex];
		}
	}
}

void Texture::loadDXT3(std::vector<uint32>& outData) {
	int numBlocksFull = (mHeader.width * mHeader.height) / 16;
	bool partialBlock = ((mHeader.width * mHeader.height) % 16) != 0;

	std::vector<uint32> blockData((numBlocksFull + (partialBlock ? 1 : 0)) * 16);

	for (int32 i = 0; i < numBlocksFull; ++i) {
		dxt3GetBlock(blockData.data() + i * 16);
	}

	if (partialBlock) {
		dxt3GetBlock(blockData.data() + numBlocksFull * 16);
	}

	for (uint32 y = 0; y < mHeader.height; ++y) {
		for (uint32 x = 0; x < mHeader.width; ++x) {
			int bx = x / 4;
			int by = y / 4;

			int ibx = x % 4;
			int iby = y % 4;

			int blockIndex = by * (mHeader.width / 4) + bx;
			int innerIndex = iby * 4 + ibx;
			outData[y * mHeader.width + x] = blockData[blockIndex * 16 + innerIndex];
		}
	}
}

void Texture::loadDXT5(std::vector<uint32>& outData) {
	int numBlocksFull = (mHeader.width * mHeader.height) / 16;
	bool partialBlock = ((mHeader.width * mHeader.height) % 16) != 0;

	std::vector<uint32> blockData((numBlocksFull + (partialBlock ? 1 : 0)) * 16);

	for (int32 i = 0; i < numBlocksFull; ++i) {
		dxt5GetBlock(blockData.data() + i * 16);
	}

	if (partialBlock) {
		dxt5GetBlock(blockData.data() + numBlocksFull * 16);
	}

	for (uint32 y = 0; y < mHeader.height; ++y) {
		for (uint32 x = 0; x < mHeader.width; ++x) {
			int bx = x / 4;
			int by = y / 4;

			int ibx = x % 4;
			int iby = y % 4;

			int blockIndex = by * (mHeader.width / 4) + bx;
			int innerIndex = iby * 4 + ibx;
			outData[y * mHeader.width + x] = blockData[blockIndex * 16 + innerIndex];
		}
	}
}

void Texture::dxt1GetBlock(uint32* colorPtr) {
	__declspec(thread) static uint8 rgbTmpArray[4][4] = {
		0, 0, 0, 255,
		0, 0, 0, 255,
		0, 0, 0, 255,
		0, 0, 0, 255
	};

	__declspec(thread) static uint16 tableIndices[16] = { 0 };

	auto& strm = *mStream;

	uint16 color1, color2;
	uint32 indices;
	if (!strm.read(color1) || !strm.read(color2) || !strm.read(indices)) {
		for (int i = 0; i < 16; i++) {
			colorPtr[i] = 0x00000000;
		}
		return;
	}

	RGB565ToRGB8Array(color1, rgbTmpArray[0]);
	RGB565ToRGB8Array(color2, rgbTmpArray[1]);

	uint8* clr1 = rgbTmpArray[0];
	uint8* clr2 = rgbTmpArray[1];
	uint8* clr3 = rgbTmpArray[2];
	uint8* clr4 = rgbTmpArray[3];

	if (color1 > color2) {
		for (int i = 0; i < 3; ++i) {
			clr4[i] = (clr1[i] + 2 * clr2[i]) / 3;
			clr3[i] = (2 * clr1[i] + clr2[i]) / 3;
		}
	} else {
		for (int i = 0; i < 3; ++i) {
			clr3[i] = (clr1[i] + clr2[i]) / 2;
			clr4[i] = 0;
		}
	}

	for (uint32 i = 0; i < 16; ++i) {
		tableIndices[i] = ((indices >> (2 * i)) & 3);
	}

	for (int y = 0; y < 4; ++y) {
		for (int x = 0; x < 4; ++x) {
			int index = y * 4 + x;
			colorPtr[index] = *(uint32*) (rgbTmpArray[tableIndices[index]]);
		}
	}
}

void Texture::dxt3GetBlock(uint32* colorPtr) {
	__declspec(thread) static uint32 alphaValues[16] = { 0 };
	__declspec(thread) static uint8 rgbTmpArray[4][4] = { { 0, 0, 0, 255 }, { 0, 0, 0, 255 }, { 0, 0, 0, 255 }, { 0, 0, 0, 255 } };
	__declspec(thread) static uint16 tableIndices[16] = { 0 };

	auto& strm = *mStream;

	uint64 alpha;
	uint16 color1, color2;
	uint32 indices;

	if (!strm.read(alpha) || !strm.read(color1) || !strm.read(color2) || !strm.read(indices)) {
		for (int i = 0; i < 16; i++) {
			colorPtr[i] = 0x00000000;
		}
		return;
	}

	for (int i = 0; i < 16; ++i) {
		alphaValues[i] = (uint8) ((((alpha >> (4 * i)) & 0x0F) / 15.0f) * 255.0f);
	}

	RGB565ToRGB8Array(color1, rgbTmpArray[0]);
	RGB565ToRGB8Array(color2, rgbTmpArray[1]);

	uint8* clr1 = rgbTmpArray[0];
	uint8* clr2 = rgbTmpArray[1];
	uint8* clr3 = rgbTmpArray[2];
	uint8* clr4 = rgbTmpArray[3];

	if (color1 > color2) {
		for (int i = 0; i < 3; ++i) {
			clr4[i] = (clr1[i] + 2 * clr2[i]) / 3;
			clr3[i] = (2 * clr1[i] + clr2[i]) / 3;
		}
	} else {
		for (int i = 0; i < 3; ++i) {
			clr3[i] = (clr1[i] + clr2[i]) / 2;
			clr4[i] = 0;
		}
	}

	for (uint32 i = 0; i < 16; ++i) {
		tableIndices[i] = ((indices >> (2 * i)) & 3);
	}

	for (int y = 0; y < 4; ++y) {
		for (int x = 0; x < 4; ++x) {
			int index = y * 4 + x;
			uint32 color = *(uint32*) (rgbTmpArray[tableIndices[index]]);
			uint32 alpha = alphaValues[index];
			color &= 0x00FFFFFF;
			color |= (alpha << 24);
			colorPtr[index] = color;
		}
	}
}

void Texture::dxt5GetBlock(uint32* colorPtr) {
	__declspec(thread) static uint8 alphaValues[8] = { 0 };
	__declspec(thread) static uint32 alphaLookup[16] = { 0 };
	__declspec(thread) static uint8 rgbTmpArray[4][4] = { { 0, 0, 0, 255 }, { 0, 0, 0, 255 }, { 0, 0, 0, 255 }, { 0, 0, 0, 255 } };
	__declspec(thread) static uint16 tableIndices[16] = { 0 };

	auto& strm = *mStream;

	uint8 alpha1, alpha2;
	uint32 lookupValue1;
	uint16 lookupValue2;
	uint16 color1, color2;
	uint32 indices;

	if (!strm.read(alpha1) || !strm.read(alpha2) || !strm.read(lookupValue1) || !strm.read(lookupValue2)
		|| !strm.read(color1) || !strm.read(color2) || !strm.read(indices)) {
		for (int i = 0; i < 16; i++) {
			colorPtr[i] = 0x00000000;
		}
		return;
	}

	alphaValues[0] = alpha1;
	alphaValues[1] = alpha2;

	if (alpha1 > alpha2) {
		for (int i = 0; i < 6; ++i) {
			alphaValues[i + 2] = (uint8) (((6.0f - i) * alpha1 + (1.0f + i) * alpha2) / 7.0f);
		}
	} else {
		for (int i = 0; i < 4; ++i) {
			alphaValues[i + 2] = (uint8) (((4.0f - i) * alpha1 + (1.0f + i) * alpha2) / 5.0f);
		}

		alphaValues[5] = 0;
		alphaValues[6] = 255;
	}

	uint64 lookupValue = lookupValue1 | ((uint64)lookupValue2) << 32;
	//uint64 lookupValue = mStream->read<uint32>();
	//lookupValue |= ((uint64) mStream->read<uint16>()) << 32;

	for (int i = 0; i < 16; ++i) {
		alphaLookup[i] = (lookupValue >> (i * 3)) & 7;
	}

	RGB565ToRGB8Array(color1, rgbTmpArray[0]);
	RGB565ToRGB8Array(color2, rgbTmpArray[1]);

	uint8* clr1 = rgbTmpArray[0];
	uint8* clr2 = rgbTmpArray[1];
	uint8* clr3 = rgbTmpArray[2];
	uint8* clr4 = rgbTmpArray[3];

	if (color1 > color2) {
		for (int i = 0; i < 3; ++i) {
			clr4[i] = (clr1[i] + 2 * clr2[i]) / 3;
			clr3[i] = (2 * clr1[i] + clr2[i]) / 3;
		}
	} else {
		for (int i = 0; i < 3; ++i) {
			clr3[i] = (clr1[i] + clr2[i]) / 2;
			clr4[i] = 0;
		}
	}

	for (uint32 i = 0; i < 16; ++i) {
		tableIndices[i] = ((indices >> (2 * i)) & 3);
	}

	for (int y = 0; y < 4; ++y) {
		for (int x = 0; x < 4; ++x) {
			int index = y * 4 + x;
			uint32 color = *(uint32*) (rgbTmpArray[tableIndices[index]]);
			uint32 alpha = alphaValues[alphaLookup[index]];
			color &= 0x00FFFFFF;
			color |= (alpha << 24);
			colorPtr[index] = color;
		}
	}
}

//void Texture::saveToFile(const std::wstring& file) {
//	glBindTexture(GL_TEXTURE_2D, mTexture);
//	GLint width = 0, height = 0;
//	glGetTexLevelParameteriv(GL_TEXTURE_2D, 0, GL_TEXTURE_WIDTH, &width);
//	glGetTexLevelParameteriv(GL_TEXTURE_2D, 0, GL_TEXTURE_HEIGHT, &height);
//	if (width == 0 || height == 0) {
//		return;
//	}
//
//	std::vector<uint32> colors(width * height);
//	glGetTexImage(GL_TEXTURE_2D, 0, GL_BGRA, GL_UNSIGNED_BYTE, colors.data());
//	glBindTexture(GL_TEXTURE_2D, 0);
//
//	static ULONG_PTR gdiToken = 0xFFFFFFFF;
//	if (gdiToken == 0xFFFFFFFF) {
//		Gdiplus::GdiplusStartupInput input;
//		Gdiplus::GdiplusStartup(&gdiToken, &input, nullptr);
//	}
//
//	Gdiplus::Bitmap bmp(width, height, PixelFormat32bppARGB);
//	Gdiplus::BitmapData data;
//	bmp.LockBits(&Gdiplus::Rect(0, 0, width, height), 0, PixelFormat32bppARGB, &data);
//	memcpy(data.Scan0, colors.data(), colors.size() * 4);
//	bmp.UnlockBits(&data);
//
//	CLSID encoder;
//	if (GetEncoderClsid(L"image/png", &encoder) < 0) {
//		return;
//	}
//
//	bmp.Save(file.c_str(), &encoder);
//}