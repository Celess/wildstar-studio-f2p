#include "stdafx.h"
#include "UIManager.h"

using namespace Awesomium;

UIManagerPtr UIManager::gInstance = nullptr;

UIManager::UIManager() {
	std::wstringstream strm;
	wchar_t curDir[MAX_PATH];
	GetCurrentDirectory(MAX_PATH, curDir);
	strm << curDir << L"\\bin";

	std::wstringstream childPath;
	childPath << curDir << L"\\bin\\" << L"awesomium_process.exe";

	WebConfig cfg;
	cfg.child_process_path = WebString((const wchar16*) (childPath.str().c_str()));
	cfg.package_path = WebString((const wchar16*) (strm.str().c_str()));
	cfg.plugin_path = cfg.package_path;
	cfg.log_level = LogLevel::kLogLevel_Verbose;
	cfg.log_path = WSLit("./debug.txt");

	mCore = WebCore::Initialize(cfg);

	glGenTextures(1, &mTexture);
}

void UIManager::OnAddConsoleMessage(Awesomium::WebView* caller, const Awesomium::WebString& message, int line_number, const Awesomium::WebString& source) {
	std::wstring msg((const wchar_t*) message.data(), message.length());
	std::wstring file((const wchar_t*) source.data(), source.length());

	MessageBoxW(nullptr, msg.c_str(), file.c_str(), MB_OK);
}

void UIManager::init(uint32 width, uint32 height, WindowPtr wnd) {
	mWindow = wnd;
	mWidth = width;
	mHeight = height;

	wnd->addMessageHandler(
		[this](UINT uMsg, WPARAM wParam, LPARAM lParam, bool& handled, LRESULT& res) {
			handled = false;

			switch (uMsg) {
			case WM_LBUTTONDOWN:
				mView->InjectMouseDown(MouseButton::kMouseButton_Left);
				break;

			case WM_RBUTTONDOWN:
				mView->InjectMouseDown(MouseButton::kMouseButton_Right);
				break;

			case WM_MBUTTONDOWN:
				mView->InjectMouseDown(MouseButton::kMouseButton_Middle);
				break;

			case WM_LBUTTONUP:
				mView->InjectMouseUp(MouseButton::kMouseButton_Left);
				break;

			case WM_RBUTTONUP:
				mView->InjectMouseUp(MouseButton::kMouseButton_Right);
				break;

			case WM_MBUTTONUP:
				mView->InjectMouseUp(MouseButton::kMouseButton_Middle);
				break;

			case WM_MOUSEMOVE:
				//OnChangeCursor(mView, mCursor);
				mView->InjectMouseMove(GET_X_LPARAM(lParam), GET_Y_LPARAM(lParam));
				break;

			case WM_MOUSEWHEEL:
				mView->InjectMouseWheel(GET_WHEEL_DELTA_WPARAM(wParam), 0);
				break;

			case WM_SETCURSOR:
				//OnChangeCursor(mView, mCursor);
				handled = false;
				res = TRUE;
				break;

			case WM_SIZE:
				{
					mWidth = GET_X_LPARAM(lParam);
					mHeight = GET_Y_LPARAM(lParam);
					mView->Resize(mWidth, mHeight);
					glViewport(0, 0, mWidth, mHeight);
				}
				break;

			case WM_CHAR:
			case WM_SYSKEYDOWN:
			case WM_SYSKEYUP:
			case WM_KEYUP:
			case WM_KEYDOWN:
				{
					WebKeyboardEvent ev(uMsg, wParam, lParam);
					mView->InjectKeyboardEvent(ev);
					break;
				}
			}
		}
	);

	mSession = mCore->CreateWebSession(WSLit(""), WebPreferences());
	mSession->AddDataSource(WSLit("local"), &mResFileMgr);

	mView = mCore->CreateWebView(width, height, mSession);

	glBindTexture(GL_TEXTURE_2D, mTexture);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);

	uint32 colors[] = { 0xFFFF0000, 0xFF00FF00, 0xFF0000FF, 0x00000000 };
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 2, 2, 0, GL_RGBA, GL_UNSIGNED_BYTE, colors);
	glBindTexture(GL_TEXTURE_2D, 0);

	mJSHandler = std::make_shared<JSHandler>(mView);

	mView->set_js_method_handler(mJSHandler.get());
	mView->set_view_listener(this);

	mView->SetTransparent(false);
	mView->LoadURL(WebURL(WSLit("asset://local/index.html")));
}

void UIManager::onFrame() {
	mCore->Update();

	BitmapSurface* bms = (BitmapSurface*) mView->surface();
	if (bms == nullptr) {
		return;
	}

	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, mTexture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, bms->width(), bms->height(), 0, GL_BGRA, GL_UNSIGNED_BYTE, bms->buffer());

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glBegin(GL_TRIANGLE_FAN);
	glTexCoord2f(0, 0);
	glVertex2f(-1, 1);

	glTexCoord2f(1, 0);
	glVertex2f(1, 1);

	glTexCoord2f(1, 1);
	glVertex2f(1, -1);

	glTexCoord2f(0, 1);
	glVertex2f(-1, -1);

	glEnd();

	glDisable(GL_BLEND);
}

void UIManager::shutdown() {

}