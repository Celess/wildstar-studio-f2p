#pragma once

#include "DataLoader.h"

SHARED_FWD(Window);

class Window
{
	static WindowPtr gInstance;

	WNDCLASS mClass;
	HWND mWindow;
	std::list<std::function<void(UINT, WPARAM, LPARAM, bool&, LRESULT&)>> mMessageHandlers;
	std::list<std::function<void ()>> mSyncCallbacks;
	std::mutex mSyncLock;
	DataLoader mLoader;

	static BOOL WINAPI WndProc(HWND hWindow, UINT uMsg, WPARAM wParam, LPARAM lParam);

	BOOL onMessage(HWND hWindow, UINT uMsg, WPARAM wParam, LPARAM lParam);
public:
	Window();

	bool parseMessages();

	void syncRunLoop();

	void addMessageHandler(std::function<void(UINT, WPARAM, LPARAM, bool&, LRESULT&)> handler) { mMessageHandlers.push_back(handler); }

	HWND getHandle() const { return mWindow; }

	void pushSyncAction(std::function<void ()> fun) { std::lock_guard<std::mutex> l(mSyncLock); mSyncCallbacks.push_back(fun); }
	void setLoadProgress(uint32 curVal, uint32 maxVal);
	void setLoadProgress(uint32 curval);
	void setLoadAction(const std::wstring& action);

	uint32 getWidth() const;
	uint32 getHeight() const;

	static WindowPtr getInstance() { 
		if (gInstance == nullptr) {
			gInstance = std::make_shared<Window>();
		}
		return gInstance; 
	}
};

#define sWindow (Window::getInstance())