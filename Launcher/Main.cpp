#include "stdafx.h"
#include "HttpRequest.h"

#include "Window.h"

void tryUpdateLauncher() {
	uint32 res = ERROR_SUCCESS;
	
	int numArgs = 0;
	LPWSTR* args = CommandLineToArgvW(GetCommandLine(), &numArgs);

	std::wstring destFolder = args[2];
	std::wstring destFile = destFolder;
	destFile += L"\\Launcher.exe";

	do {
		res = DeleteFile(destFile.c_str());
	} while (res == ERROR_ACCESS_DENIED);

	wchar_t curDir[MAX_PATH] = { L'\0' };
	GetCurrentDirectory(MAX_PATH, curDir);

	HMODULE curMod = GetModuleHandle(nullptr);
	wchar_t modulePath[MAX_PATH] = { L'\0' };
	GetModuleFileName(curMod, modulePath, MAX_PATH);

	MoveFile(modulePath, destFile.c_str());
	ShellExecute(nullptr, L"open", destFile.c_str(), L"", destFolder.c_str(), SW_SHOW);
}

BOOL WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, INT) {
	std::wstring cmdLine = GetCommandLine();
	if (cmdLine.find(L"--update") != std::wstring::npos) {
		tryUpdateLauncher();
		return TRUE;
	}

	curl_global_init(CURL_GLOBAL_DEFAULT);

	LoadLibrary(L"Riched20.dll");

	//HttpRequest req(L"https://bitbucket.org/mugadr_m/wildstar-studio/downloads/VersionInfo.txt");
	//std::wstring resp = req.getResponseSync();
	CoInitialize(nullptr);
	ULONG_PTR gdiToken = 0;
	Gdiplus::GdiplusStartupInput startInput;
	Gdiplus::GdiplusStartup(&gdiToken, &startInput, nullptr);

	sWindow->syncRunLoop();

	Gdiplus::GdiplusShutdown(gdiToken);

	CoUninitialize();

	return TRUE;
}