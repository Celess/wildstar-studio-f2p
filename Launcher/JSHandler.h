#pragma once

class JSHandler : public Awesomium::JSMethodHandler
{
private:
	std::map<int32, std::map<std::wstring, std::function<void (const Awesomium::JSArray&)>>> mNonRetFunctions;
	std::map<int32, std::map<std::wstring, std::function<Awesomium::JSValue (const Awesomium::JSArray&)>>> mRetFunctions;

	void OnMethodCall(Awesomium::WebView* caller, unsigned int remote_object_id, const Awesomium::WebString& method_name, const Awesomium::JSArray& args);
	Awesomium::JSValue OnMethodCallWithReturnValue(Awesomium::WebView* caller, unsigned int remote_object_id, const Awesomium::WebString& method_name, const Awesomium::JSArray& args);

public:
	JSHandler(Awesomium::WebView* view);
};

SHARED_TYPE(JSHandler);