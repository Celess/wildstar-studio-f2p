#include "stdafx.h"
#include "JSHandler.h"
#include "HttpRequest.h"

using namespace Awesomium;

std::wstring escapeJsonString(const std::wstring& input) {
	std::wostringstream ss;
	for (auto iter = input.cbegin(); iter != input.cend(); iter++) {
		switch (*iter) {
		case '\\': ss << "\\\\"; break;
		case '"': ss << "\\\""; break;
		case '/': ss << "\\/"; break;
		case '\b': ss << "\\b"; break;
		case '\f': ss << "\\f"; break;
		case '\n': ss << "\\n"; break;
		case '\r': ss << "\\r"; break;
		case '\t': ss << "\\t"; break;
		default: ss << *iter; break;
		}
	}
	return ss.str();
}

JSHandler::JSHandler(WebView* view) {
	auto apiVal = view->CreateGlobalJavascriptObject(WSLit("API"));
	auto webVal = view->CreateGlobalJavascriptObject(WSLit("API.web"));

	auto& webObj = webVal.ToObject();

	mNonRetFunctions[webObj.remote_id()][L"pageLoaded"] = [this, view](const JSArray&) {
		std::wstringstream strm;
		strm << L"https://bitbucket.org/mugadr_m/wildstar-studio/downloads/index.html?version=" << rand();
		HttpRequest req(strm.str());
		std::wstring resp = req.getResponseSync();

		JSValue res = view->ExecuteJavascriptWithResult(WSLit("var obj = { callback: function(code) { onIframeLoaded(code); } }; obj;"), WSLit(""));
		JSArray args;
		args.Push(JSValue(WebString((const wchar16*) resp.c_str())));
		res.ToObject().Invoke(WSLit("callback"), args);
	};

	webObj.SetCustomMethod(WSLit("pageLoaded"), false);
}

void JSHandler::OnMethodCall(Awesomium::WebView* caller, unsigned int remote_object_id, const Awesomium::WebString& method_name, const Awesomium::JSArray& args) {
	auto itr = mNonRetFunctions.find(remote_object_id);
	if (itr == mNonRetFunctions.end()) {
		return;
	}

	std::wstring method((const wchar_t*) method_name.data(), method_name.length());
	auto fitr = itr->second.find(method);
	if (fitr != itr->second.end()) {
		fitr->second(args);
	}
}

Awesomium::JSValue JSHandler::OnMethodCallWithReturnValue(Awesomium::WebView* caller, unsigned int remote_object_id, const Awesomium::WebString& method_name, const Awesomium::JSArray& args) {
	auto itr = mRetFunctions.find(remote_object_id);
	if (itr == mRetFunctions.end()) {
		return JSValue::Undefined();
	}

	std::wstring method((const wchar_t*) method_name.data(), method_name.length());
	auto fitr = itr->second.find(method);
	if (fitr != itr->second.end()) {
		return fitr->second(args);
	}

	return JSValue::Undefined();
}