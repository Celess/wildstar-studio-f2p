#include "stdafx.h"
#include "ResourceFileManager.h"

using namespace Awesomium;

ResourceFileManager::ResourceFileManager() {

}

void ResourceFileManager::OnRequest(int request_id, const WebString& path) {
	std::wstring stlPath((const wchar_t*)path.data(), path.length());
	std::replace(stlPath.begin(), stlPath.end(), L'/', L'\\');

	std::wstringstream strm;
	strm << L".\\UI\\" << stlPath;

	std::tr2::sys::wpath fpath(strm.str());
	std::wstring ext = fpath.extension();
	std::transform(ext.begin(), ext.end(), ext.begin(), tolower);

	WebString mime;
	bool isBinary = false;

	if (ext == L".js") {
		mime = WSLit("text/javascript");
	} else if (ext == L".png") {
		isBinary = true;
		mime = WSLit("image/png");
	} else if (ext == L".css") {
		mime = WSLit("text/css");
	} else if (ext == L".otf") {
		isBinary = true;
		mime = WSLit("font/opentype");
	} else if (ext == L".gif") {
		isBinary = true;
		mime = WSLit("image/gif");
	} else if (ext == L".json") {
		mime = WSLit("text/json");
	} else {
		mime = WSLit("text/html");
	}

	std::ifstream file(strm.str(), isBinary ? std::ios::binary : std::ios::in);
	if (file.is_open() == false) {
		SendResponse(request_id, 0, nullptr, WSLit(""));
		return;
	}

	std::vector<char> content;

	if (isBinary == false) {
		std::stringstream inStrm;
		std::string buffer;
		while (std::getline(file, buffer)) {
			inStrm << buffer << std::endl;
		}

		buffer = inStrm.str();
		content.resize(buffer.length());
		memset(content.data(), 0, content.size());
		memcpy(content.data(), buffer.c_str(), buffer.length());
	} else {
		file.seekg(0, std::ios::end);
		uint32 len = (uint32) file.tellg();
		file.seekg(0, std::ios::beg);
		content.resize(len);
		file.read(content.data(), len);
	}

	SendResponse(request_id, content.size(), (unsigned char*)content.data(), mime);
}