#include "stdafx.h"
#include "Window.h"
#include "resource.h"
#include "HttpRequest.h"

#pragma comment(linker,"\"/manifestdependency:type='win32' \
name='Microsoft.Windows.Common-Controls' version='6.0.0.0' \
processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")

WindowPtr Window::gInstance = nullptr;

Window::Window() {
	mWindow = CreateDialogParam(GetModuleHandle(nullptr), MAKEINTRESOURCE(IDD_DIALOG1), GetDesktopWindow(), &Window::WndProc, (LPARAM) this);
	SendMessage(mWindow, WM_SETICON, ICON_BIG, (LPARAM)LoadIcon(GetModuleHandle(nullptr), MAKEINTRESOURCE(IDI_ICON1)));
	SendMessage(mWindow, WM_SETICON, ICON_SMALL, (LPARAM) LoadIcon(GetModuleHandle(nullptr), MAKEINTRESOURCE(IDI_ICON1)));
}

bool Window::parseMessages() {
	MSG msg;
	while (PeekMessage(&msg, nullptr, 0, 0, PM_REMOVE)) {
		if (msg.message == WM_QUIT) {
			return false;
		}

		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	return true;
}

void Window::syncRunLoop() {
	ShowWindow(mWindow, SW_SHOW);

	mLoader.initLoading();

	while (parseMessages()) {
		{
			std::lock_guard<std::mutex> l(mSyncLock);
			for (auto& cb : mSyncCallbacks) {
				cb();
			}

			mSyncCallbacks.clear();
		}
		std::this_thread::sleep_for(std::chrono::milliseconds(10));
	}
}

uint32 Window::getWidth() const {
	RECT rc;
	GetClientRect(mWindow, &rc);
	return rc.right - rc.left;
}

uint32 Window::getHeight() const {
	RECT rc;
	GetClientRect(mWindow, &rc);
	return rc.bottom - rc.top;
}

BOOL Window::onMessage(HWND hWindow, UINT uMsg, WPARAM wParam, LPARAM lParam) {
	LRESULT resOverride = 0;
	bool globalHandled = false;
	for (auto& handler : mMessageHandlers) {
		bool handled = false;
		LRESULT res;
		handler(uMsg, wParam, lParam, handled, res);
		if (handled) {
			globalHandled = true;
			resOverride = res;
		}
	}

	if (uMsg == WM_COMMAND) {
		switch (HIWORD(wParam)) {
		case BN_CLICKED:
			if (LOWORD(wParam) == IDC_BUTTON1) {
				wchar_t curDir[MAX_PATH] = { L'\0' };
				GetCurrentDirectory(MAX_PATH, curDir);
				ShellExecute(nullptr, L"open", L"WildstarStudio.exe", L"", curDir, SW_SHOW);
				ExitProcess(0);
			}
			break;
		}
	}

	if (uMsg == WM_INITDIALOG) {
		mWindow = hWindow;
	}

	if (uMsg == WM_CLOSE) {
		PostQuitMessage(0);
		return TRUE;
	}

	if (globalHandled) {
		return TRUE;
	}

	return FALSE;
}

BOOL WINAPI Window::WndProc(HWND hWindow, UINT uMsg, WPARAM wParam, LPARAM lParam) {
	Window* wnd = (Window*) GetProp(hWindow, L"PROP_WND_INSTANCE");
	if (wnd != nullptr) {
		return wnd->onMessage(hWindow, uMsg, wParam, lParam);
	}

	if (uMsg == WM_INITDIALOG) {
		wnd = (Window*) lParam;
		SetProp(hWindow, L"PROP_WND_INSTANCE", (HANDLE) wnd);
		return wnd->onMessage(hWindow, uMsg, wParam, lParam);
	}

	return FALSE;
}

void Window::setLoadProgress(uint32 curVal, uint32 maxVal) {
	pushSyncAction([this, curVal, maxVal]() {
		SendDlgItemMessage(mWindow, IDC_LOAD_PROGRESS, PBM_SETRANGE, 0, maxVal << 16);
		SendDlgItemMessage(mWindow, IDC_LOAD_PROGRESS, PBM_SETPOS, curVal, 0);
	});
}

void Window::setLoadProgress(uint32 curVal) {
	pushSyncAction([this, curVal]() {
		SendDlgItemMessage(mWindow, IDC_LOAD_PROGRESS, PBM_SETPOS, curVal, 0);
	});
}

void Window::setLoadAction(const std::wstring& action) {
	pushSyncAction([this, action]() {
		SetDlgItemText(mWindow, IDC_CUR_ACTION, action.c_str());
	});
}